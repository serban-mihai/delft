# Delft

1 |   Create a Python 3.x script called ‘HoeWarmIsHetInDelft.py’ that retrieves from the
Internet the current temperature in Delft and prints it to standard output, rounded to
degrees Celsius. Obtain the information from http://www.weerindelft.nl/

User types:
python HoeWarmIsHetInDelft.py
Response:
18 degrees Celsius

2 | Package the code above in a docker container
3 | Create a gitlab-ci.yml pipeline  that installs the container above and  executes the script
We’ll review the code based on clarity, correctness, robustness, programming patterns used and elegance on implementation.
(Please use gitlab.com if local installation of gitlab and ci runner is skipped )
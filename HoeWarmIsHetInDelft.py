# Luxoft Test
# The http://www.weerindelft.nl website uses an AJAX call to fetch data about the temperature in Delft
# and send it back to the client. Raw results of this call can be found on the following endpoint: 
# https://weerindelft.nl/clientraw.txt?1605017953212 (Note: the timestamp query param can be omitted)

import aiohttp
import asyncio
import time

__author__ = "Serban Mihai-Ciprian"

DOMAIN = "https://weerindelft.nl"
ENDPOINT = "clientraw.txt"

async def fetch(session, url):
    async with session.get(url) as response:
        return await response.text()

async def main():
    timestamp = time.time()
    async with aiohttp.ClientSession() as session:
        response = await fetch(session, f"{DOMAIN}/{ENDPOINT}?{int(timestamp)}")
        current_temp = list(response.split(" "))[4]     # The 4th element of the list will be the current temperature
        print(f"{current_temp} degrees Celsius")

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

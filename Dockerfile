FROM python:3.9.0-alpine

WORKDIR /delft

COPY requirements.txt .
COPY HoeWarmIsHetInDelft.py .

RUN apk add libc-dev gcc
RUN pip install -r requirements.txt

CMD [ "python", "./HoeWarmIsHetInDelft.py" ] 